# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

HOMEPAGE = "https://docs.oniroproject.org/"
SUMMARY = "Flutter blueprint image"
DESCRIPTION = "The Flutter image is composed of homescreen demo application"
LICENSE = "Apache-2.0"

require recipes-core/images/oniro-image-base.bb

REQUIRED_DISTRO_FEATURES = "\
	opengl \
        systemd \
	wayland \
	"

# TODO: update home in /tmp ?
IMAGE_FEATURES:remove = "read-only-rootfs"
ROOT_FSTYPE = "ext4"

IMAGE_INSTALL:append = "\
	flutter-engine-${FLUTTER_RUNTIME} \
	ivi-homescreen-${FLUTTER_RUNTIME} \
 	flutter-gallery-${FLUTTER_RUNTIME} \
        weston \
        weston-init \
	"

IMAGE_INSTALL_RASPBERRYPI = "\
       flutter-pi-${FLUTTER_RUNTIME} \
       flutter-drm-gbm-backend \
       libdrm-tests \
       "

IMAGE_INSTALL:append:raspberrypi3 = "${IMAGE_INSTALL_RASPBERRYPI}"
IMAGE_INSTALL:append:raspberrypi3-64 = "${IMAGE_INSTALL_RASPBERRYPI}"
IMAGE_INSTALL:append:raspberrypi4-64 = "${IMAGE_INSTALL_RASPBERRYPI}"

PREFERRED_PROVIDER:jpeg = "libjpeg-turbo"
PREFERRED_PROVIDER:jpeg-native = "libjpeg-turbo-native"

QB_MEM = '${@bb.utils.contains("DISTRO_FEATURES", "opengl", "-m 512", "-m 256", d)}'

# TODO: extra application for debuging purposes
IMAGE_INSTALL:append = "\
       avahi-daemon \
       gdb \
       os-release \
       screen \
       strace \
       zile \
"
